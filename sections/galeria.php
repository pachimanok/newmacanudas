<section id="gallery" class="gallery">
    <div class="container-fluid">
        <div class="section-title">
            <h2>Algunas imágenes de nuestros <span>Productos</span></h2>
            <p>Sabores que compartimos</p>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="http://localhost/laboratoriomacanudo/assets/img/gallery/gallery-2.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-20.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="assets/img/gallery/gallery-3.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-44.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="assets/img/gallery/gallery-1.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-106.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="assets/img/gallery/gallery-4.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-15.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="assets/img/gallery/gallery-5.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-64.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="assets/img/gallery/gallery-6.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-67.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="assets/img/gallery/gallery-7.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-74.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="gallery-item">
                    <a href="assets/img/gallery/gallery-8.jpg" class="venobox" data-gall="gallery-item">
                        <img src="assets/img/slide/Galeria/Macanudas-92.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section><!-- End Gallery Section -->